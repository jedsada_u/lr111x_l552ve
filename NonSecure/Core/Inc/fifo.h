/**
 * @file      fifo.h
 *
 * @brief     FIFO buffer implementation
 *
 * @copyright Revised BSD License, see section \ref LICENSE.
 *
 * @code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * @endcode
 *
 * @author    Miguel Luis ( Semtech )
 *
 * @author    Gregory Cristian ( Semtech )
 */

#ifndef __FIFO_H__
#define __FIFO_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

/**
 * FIFO structure
 */
typedef struct Fifo_s
{
	uint16_t Begin;
	uint16_t End;
	uint8_t *Data;
	uint16_t Size;
} Fifo_t;

/**
 * @brief     Initializes the FIFO structure
 * @param[in] fifo   Pointer to the FIFO object
 * @param[in] buffer Buffer to be used as FIFO
 * @param[in] size   Size of the buffer
 */
void FifoInit(Fifo_t *fifo, uint8_t *buffer, uint16_t size);

/**
 * @brief     Pushes data to the FIFO
 * @param[in] fifo Pointer to the FIFO object
 * @param[in] data Data to be pushed into the FIFO
 */
void FifoPush(Fifo_t *fifo, uint8_t data);

/**
 * @brief     Pops data from the FIFO
 * @param[in] fifo Pointer to the FIFO object
 * @return    data popped from the FIFO
 */
uint8_t FifoPop(Fifo_t *fifo);

/**
 * @brief     Flushes the FIFO
 * @param[in] fifo Pointer to the FIFO object
 */
void FifoFlush(Fifo_t *fifo);

/**
 * @brief     Checks if the FIFO is empty
 * @param[in] fifo Pointer to the FIFO object
 * @return    isEmpty true: FIFO is empty, false FIFO is not empty
 */
bool IsFifoEmpty(Fifo_t *fifo);

/**
 * @brief     Checks if the FIFO is full
 * @param[in] fifo   Pointer to the FIFO object
 * @return    isFull true: FIFO is full, false FIFO is not full
 */
bool IsFifoFull(Fifo_t *fifo);

#ifdef __cplusplus
}
#endif

#endif /* __FIFO_H__ */

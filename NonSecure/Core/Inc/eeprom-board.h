/**
 * @file      eeprom-board.h
 *
 * @brief     Target board EEPROM driver implementation
 *
 * @copyright Revised BSD License, see section \ref LICENSE.
 *
 * @code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * @endcode
 *
 * @author    Miguel Luis ( Semtech )
 *
 * @author    Gregory Cristian ( Semtech )
 */

#ifndef __EEPROM_BOARD_H__
#define __EEPROM_BOARD_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/**
 * @brief     Writes the given buffer to the EEPROM at the specified address.
 * @param[in] addr EEPROM address to write to
 * @param[in] buffer Pointer to the buffer to be written.
 * @param[in] size Size of the buffer to be written.
 * @return    status [SUCCESS, FAIL]
 */
uint8_t EepromMcuWriteBuffer(uint16_t addr, uint8_t *buffer, uint16_t size);

/**
 * @brief     Reads the EEPROM at the specified address to the given buffer.
 * @param[in] addr EEPROM address to read from
 * @param[in] buffer Pointer to the buffer to be written with read data.
 * @param[in] size Size of the buffer to be read.
 * @return    status [SUCCESS, FAIL]
 */
uint8_t EepromMcuReadBuffer(uint16_t addr, uint8_t *buffer, uint16_t size);

/**
 * @brief Sets the device address.
 * @param[in] addr External EEPROM address
 */
void EepromMcuSetDeviceAddr(uint8_t addr);

/**
 * @brief  Gets the current device address.
 * @remark Useful for I2C external EEPROMS
 * @return addr External EEPROM address
 */
uint8_t EepromMcuGetDeviceAddr(void);

#ifdef __cplusplus
}
#endif

#endif /* __EEPROM_BOARD_H__ */

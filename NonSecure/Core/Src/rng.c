/**
 ******************************************************************************
 * @file    rng.c
 * @brief   This file provides code for the configuration
 *          of the RNG instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "rng.h"

/* Private typedef -----------------------------------------------------------*/
RNG_HandleTypeDef hrng;

/* RNG init function */
void MX_RNG_Init(void)
{
    hrng.Instance                 = RNG;
    hrng.Init.ClockErrorDetection = RNG_CED_ENABLE;
    if (HAL_RNG_Init(&hrng) != HAL_OK)
    {
        Error_Handler();
    }
}

/* HAL RNG MCU Support package init function */
void HAL_RNG_MspInit(RNG_HandleTypeDef *rngHandle)
{

    RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };
    if (rngHandle->Instance == RNG)
    {
        /*
         * Initializes the peripherals clock
         */
        PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RNG;
        PeriphClkInit.RngClockSelection    = RCC_RNGCLKSOURCE_HSI48;
        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            Error_Handler();
        }

        /* RNG clock enable */
        __HAL_RCC_RNG_CLK_ENABLE();

        /* RNG interrupt Init */
        HAL_NVIC_SetPriority(RNG_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(RNG_IRQn);
    }
}

/* HAL RNG MCU Support package deinit function */
void HAL_RNG_MspDeInit(RNG_HandleTypeDef *rngHandle)
{
    if (rngHandle->Instance == RNG)
    {
        /* Peripheral clock disable */
        __HAL_RCC_RNG_CLK_DISABLE();

        /* RNG interrupt Deinit */
        HAL_NVIC_DisableIRQ(RNG_IRQn);
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

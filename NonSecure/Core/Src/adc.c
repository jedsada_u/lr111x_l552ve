/**
 ******************************************************************************
 * @file    adc.c
 * @brief   This file provides code for the configuration
 *          of the ADC instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* Private typedef -----------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

/* Private variables ---------------------------------------------------------*/
static uint32_t HAL_RCC_ADC_CLK_ENABLED = 0;

/* ADC1 init function */
void MX_ADC1_Init(void)
{
    ADC_MultiModeTypeDef   multimode = { 0 };
    ADC_ChannelConfTypeDef sConfig   = { 0 };

    /*
     * Common config
     */
    hadc1.Instance                   = ADC1;
    hadc1.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;
    hadc1.Init.Resolution            = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ScanConvMode          = ADC_SCAN_DISABLE;
    hadc1.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
    hadc1.Init.LowPowerAutoWait      = DISABLE;
    hadc1.Init.ContinuousConvMode    = DISABLE;
    hadc1.Init.NbrOfConversion       = 1;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
    hadc1.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc1.Init.DMAContinuousRequests = DISABLE;
    hadc1.Init.Overrun               = ADC_OVR_DATA_PRESERVED;
    hadc1.Init.OversamplingMode      = DISABLE;
    if (HAL_ADC_Init(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }

    /*
     * Configure the ADC multi-mode
     */
    multimode.Mode = ADC_MODE_INDEPENDENT;
    if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
    {
        Error_Handler();
    }

    /*
     *  Configure Regular Channel
     */
    sConfig.Channel      = ADC_CHANNEL_VBAT;
    sConfig.Rank         = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
    sConfig.SingleDiff   = ADC_SINGLE_ENDED;
    sConfig.OffsetNumber = ADC_OFFSET_NONE;
    sConfig.Offset       = 0;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

/* ADC2 init function */
void MX_ADC2_Init(void)
{
    ADC_ChannelConfTypeDef sConfig = { 0 };

    /*
     * Common config
     */
    hadc2.Instance                   = ADC2;
    hadc2.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;
    hadc2.Init.Resolution            = ADC_RESOLUTION_12B;
    hadc2.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    hadc2.Init.ScanConvMode          = ADC_SCAN_DISABLE;
    hadc2.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
    hadc2.Init.LowPowerAutoWait      = DISABLE;
    hadc2.Init.ContinuousConvMode    = DISABLE;
    hadc2.Init.NbrOfConversion       = 1;
    hadc2.Init.DiscontinuousConvMode = DISABLE;
    hadc2.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
    hadc2.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc2.Init.DMAContinuousRequests = DISABLE;
    hadc2.Init.Overrun               = ADC_OVR_DATA_PRESERVED;
    hadc2.Init.OversamplingMode      = DISABLE;
    if (HAL_ADC_Init(&hadc2) != HAL_OK)
    {
        Error_Handler();
    }

    /*
     * Configure Regular Channel
     */
    sConfig.Channel      = ADC_CHANNEL_12;
    sConfig.Rank         = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
    sConfig.SingleDiff   = ADC_SINGLE_ENDED;
    sConfig.OffsetNumber = ADC_OFFSET_NONE;
    sConfig.Offset       = 0;
    if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

/* HAL ADC MCU Support package init function */
void HAL_ADC_MspInit(ADC_HandleTypeDef *adcHandle)
{
    GPIO_InitTypeDef         GPIO_InitStruct = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInit   = { 0 };

    if (adcHandle->Instance == ADC1)
    {
        /*
         * Initializes the peripherals clock
         */
        PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
        PeriphClkInit.AdcClockSelection    = RCC_ADCCLKSOURCE_SYSCLK;
        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            Error_Handler();
        }

        /* ADC1 clock enable */
        HAL_RCC_ADC_CLK_ENABLED++;
        if (HAL_RCC_ADC_CLK_ENABLED == 1)
        {
            __HAL_RCC_ADC_CLK_ENABLE();
        }

        __HAL_RCC_GPIOB_CLK_ENABLE();

        /*
         * ADC1 GPIO Configuration
         * PB1 ------> ADC1_IN16
         */
        GPIO_InitStruct.Pin  = EXT_AN_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(EXT_AN_GPIO_Port, &GPIO_InitStruct);

        /*
         * ADC1 DMA Init
         * ADC1 Init
         */

        /* ADC1 interrupt Init */
        HAL_NVIC_SetPriority(ADC1_2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
    }
    else if (adcHandle->Instance == ADC2)
    {
        /*
         * Initializes the peripherals clock
         */
        PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
        PeriphClkInit.AdcClockSelection    = RCC_ADCCLKSOURCE_SYSCLK;
        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            Error_Handler();
        }

        /* ADC2 clock enable */
        HAL_RCC_ADC_CLK_ENABLED++;
        if (HAL_RCC_ADC_CLK_ENABLED == 1)
        {
            __HAL_RCC_ADC_CLK_ENABLE();
        }

        __HAL_RCC_GPIOA_CLK_ENABLE();

        /*
         * ADC2 GPIO Configuration
         * PA7 ------> ADC2_IN12
         */
        GPIO_InitStruct.Pin  = CURRENT_AN_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(CURRENT_AN_GPIO_Port, &GPIO_InitStruct);

        /* ADC2 interrupt Init */
        HAL_NVIC_SetPriority(ADC1_2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
    }
}

/* HAL ADC MCU Support package deinit function */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef *adcHandle)
{
    if (adcHandle->Instance == ADC1)
    {
        /* Peripheral clock disable */
        HAL_RCC_ADC_CLK_ENABLED--;
        if (HAL_RCC_ADC_CLK_ENABLED == 0)
        {
            __HAL_RCC_ADC_CLK_DISABLE();
        }

        /*
         * ADC1 DMA DeInit
         * ADC1 GPIO Configuration
         * PB1 ------> ADC1_IN16
         */
        HAL_GPIO_DeInit(EXT_AN_GPIO_Port, EXT_AN_Pin);

        /*
         * ADC1 interrupt Deinit
         * Uncomment the line below to disable the "ADC1_2_IRQn" interrupt
         * Be aware, disabling shared interrupt may affect other IPs
         */
        /* HAL_NVIC_DisableIRQ(ADC1_2_IRQn); */
    }
    else if (adcHandle->Instance == ADC2)
    {
        /* Peripheral clock disable */
        HAL_RCC_ADC_CLK_ENABLED--;
        if (HAL_RCC_ADC_CLK_ENABLED == 0)
        {
            __HAL_RCC_ADC_CLK_DISABLE();
        }

        /*
         * ADC2 GPIO Configuration
         * PA7 ------> ADC2_IN12
         */
        HAL_GPIO_DeInit(CURRENT_AN_GPIO_Port, CURRENT_AN_Pin);

        /*
         * ADC2 interrupt Deinit
         * Uncomment the line below to disable the "ADC1_2_IRQn" interrupt
         * Be aware, disabling shared interrupt may affect other IPs
         */
        /* HAL_NVIC_DisableIRQ(ADC1_2_IRQn); */
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

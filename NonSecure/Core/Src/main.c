/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "crc.h"
#include "dma.h"
#include "gpio.h"
#include "gtzc.h"
#include "i2c.h"
#include "icache.h"
#include "rng.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "ucpd.h"
#include "usart.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
#include "CayenneLpp.h"
#include "Commissioning.h"
#include "LmHandler.h"
#include "LmHandlerMsgDisplay.h"
#include "LmhpCompliance.h"
#include "RegionCommon.h"
#include "cli.h"
#include "firmwareVersion.h"
#include "githubVersion.h"
#include "lr1110-board.h"
#include "lr1110_bootloader.h"
#include "lr1110_driver_version.h"
#include "lr1110_gnss.h"
#include "lr1110_wifi.h"
#include "radio.h"
#include "stdbool.h"
#include "stdio.h"
#include "stm32l552e_eval.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
//#define TX_CW	    1
//#define RX_SENSE	1

/* Private macro -------------------------------------------------------------*/
#ifndef ACTIVE_REGION
#warning "No active region defined, LORAMAC_REGION_EU868 will be used as default."
#define ACTIVE_REGION LORAMAC_REGION_EU868
#endif

/**
 * @brief LoRaWAN default end-device class
 */
#define LORAWAN_DEFAULT_CLASS CLASS_A

/**
 * @brief Defines the application data transmission duty cycle. 5s, value in [ms].
 */
#define APP_TX_DUTYCYCLE 5000 * 9

/**
 * @brief Defines a random delay for application data transmission duty cycle. 1s,
 *        value in [ms].
 */
#define APP_TX_DUTYCYCLE_RND 1000

/**
 * @brief  LoRaWAN Adaptive Data Rate
 * @remark Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_STATE LORAMAC_HANDLER_ADR_ON

/**
 * @brief  Default datarate
 * @remark Please note that LORAWAN_DEFAULT_DATARATE is used only when ADR is disabled
 */
#define LORAWAN_DEFAULT_DATARATE DR_2

/**
 * @brief LoRaWAN confirmed messages
 */
#define LORAWAN_DEFAULT_CONFIRMED_MSG_STATE LORAMAC_HANDLER_UNCONFIRMED_MSG

/**
 * @brief User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFFER_MAX_SIZE 242

/**
 * @brief  LoRaWAN ETSI duty cycle control enable/disable
 * @remark Please note that ETSI mandates duty cycled transmissions. Use only for test purposes
 */
#define LORAWAN_DUTYCYCLE_ON true

/**
 * @brief  LoRaWAN application port
 * @remark The allowed port range is from 1 up to 223. Other values are reserved.
 */
#define LORAWAN_APP_PORT 2

/* Private typedef -----------------------------------------------------------*/
/**
 *
 */
typedef enum
{
    LORAMAC_HANDLER_TX_ON_TIMER,
    LORAMAC_HANDLER_TX_ON_EVENT,
} LmHandlerTxEvents_t;

/* Private variables ---------------------------------------------------------*/
/**
 * User application data
 */
static uint8_t AppDataBuffer[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];

/**
 * User application data structure
 */
#if 0
static LmHandlerAppData_t AppData = {
    .Buffer     = AppDataBuffer,
    .BufferSize = 0,
    .Port       = 0,
};
#endif

/**
 * Specifies the state of the application LED
 */
static bool AppLedStateOn = false;

/**
 * Timer to handle the application data transmission duty cycle
 */
static TimerEvent_t TxTimer;

/**
 * Timer to handle the state of LED1
 */
static TimerEvent_t Led1Timer;
volatile bool       Led1TimerEvent = false;

/**
 * Timer to handle the state of LED2
 */
static TimerEvent_t Led2Timer;
volatile bool       Led2TimerEvent = false;

/**
 * Timer to handle the state of LED beacon indicator
 */
static TimerEvent_t LedBeaconTimer;

extern lr1110_t LR1110;

#if defined(TX_CW) || defined(RX_SENSE)
#define RF_FREQUENCY    923000000 // Hz
#define TX_OUTPUT_POWER 20        // 14 dBm
#define TX_TIMEOUT      100
static RadioEvents_t RadioEvents;
void                 OnRadioTxTimeout(void);
#define LORA_BANDWIDTH             0  // [0: 125 kHz,
#define LORA_SPREADING_FACTOR      10 // [SF7..SF12]
#define LORA_CODINGRATE            1  // [1: 4/5,
#define LORA_SYMBOL_TIMEOUT        5  // Symbols
#define LORA_PREAMBLE_LENGTH       8  // Same for Tx and Rx
#define LORA_FIX_LENGTH_PAYLOAD_ON false
#define LORA_IQ_INVERSION_ON       false
void           OnRadioRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr);
void           OnRadioRxTimeout(void);
static int16_t iRssi;
#endif /* TX_CW */
/* Private function prototypes -----------------------------------------------*/
void        SecureFault_Callback(void);
void        SecureError_Callback(void);
static void OnMacProcessNotify(void);
static void OnNvmDataChange(LmHandlerNvmContextStates_t state, uint16_t size);
static void OnNetworkParametersChange(CommissioningParams_t *params);
static void OnMacMcpsRequest(LoRaMacStatus_t status, McpsReq_t *mcpsReq, TimerTime_t nextTxIn);
static void OnMacMlmeRequest(LoRaMacStatus_t status, MlmeReq_t *mlmeReq, TimerTime_t nextTxIn);
static void OnJoinRequest(LmHandlerJoinParams_t *params);
static void OnTxData(LmHandlerTxParams_t *params);
static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params);
static void OnClassChange(DeviceClass_t deviceClass);
static void OnBeaconStatusChange(LoRaMacHandlerBeaconParams_t *params);
#if (LMH_SYS_TIME_UPDATE_NEW_API == 1)
static void OnSysTimeUpdate(bool isSynchronized, int32_t timeCorrection);
#else
static void OnSysTimeUpdate(void);
#endif

static void StartTxProcess(LmHandlerTxEvents_t txEvent);
#ifdef TX_CW
static void PrepareTxFrame(void);
static void UplinkProcess(void);
#endif

static void OnTxPeriodicityChanged(uint32_t periodicity);
static void OnTxFrameCtrlChanged(LmHandlerMsgTypes_t isTxConfirmed);
static void OnPingSlotPeriodicityChanged(uint8_t pingSlotPeriodicity);

/**
 * Function executed on TxTimer event
 */
static void OnTxTimerEvent(void *context);

/**
 * Function executed on Led 1 Timeout event
 */
static void OnLed1TimerEvent(void *context);

/**
 * Function executed on Led 2 Timeout event
 */
static void OnLed2TimerEvent(void *context);

/**
 * @brief Function executed on Beacon timer Timeout event
 */
static void OnLedBeaconTimerEvent(void *context);

/**
 * Radio events function pointer
 */
static LmHandlerCallbacks_t LmHandlerCallbacks = {
    .GetBatteryLevel           = BoardGetBatteryLevel,
    .GetTemperature            = NULL,
    .GetRandomSeed             = BoardGetRandomSeed,
    .OnMacProcess              = OnMacProcessNotify,
    .OnNvmDataChange           = OnNvmDataChange,
    .OnNetworkParametersChange = OnNetworkParametersChange,
    .OnMacMcpsRequest          = OnMacMcpsRequest,
    .OnMacMlmeRequest          = OnMacMlmeRequest,
    .OnJoinRequest             = OnJoinRequest,
    .OnTxData                  = OnTxData,
    .OnRxData                  = OnRxData,
    .OnClassChange             = OnClassChange,
    .OnBeaconStatusChange      = OnBeaconStatusChange,
    .OnSysTimeUpdate           = OnSysTimeUpdate,
};

static LmHandlerParams_t LmHandlerParams = {
    .Region              = ACTIVE_REGION,
    .AdrEnable           = LORAWAN_ADR_STATE,
    .IsTxConfirmed       = LORAWAN_DEFAULT_CONFIRMED_MSG_STATE,
    .TxDatarate          = LORAWAN_DEFAULT_DATARATE,
    .PublicNetworkEnable = LORAWAN_PUBLIC_NETWORK,
    .DutyCycleEnabled    = LORAWAN_DUTYCYCLE_ON,
    .DataBufferMaxSize   = LORAWAN_APP_DATA_BUFFER_MAX_SIZE,
    .DataBuffer          = AppDataBuffer,
    .PingSlotPeriodicity = REGION_COMMON_DEFAULT_PING_SLOT_PERIODICITY,
};

static LmhpComplianceParams_t LmhpComplianceParams = {
    .FwVersion.Value              = FIRMWARE_VERSION,
    .OnTxPeriodicityChanged       = OnTxPeriodicityChanged,
    .OnTxFrameCtrlChanged         = OnTxFrameCtrlChanged,
    .OnPingSlotPeriodicityChanged = OnPingSlotPeriodicityChanged,
};

/**
 * Indicates if LoRaMacProcess call is pending.
 *
 * @warning If variable is equal to 0 then the MCU can be set in low power mode
 */
static volatile uint8_t  IsMacProcessPending = 0;
static volatile uint8_t  IsTxFramePending    = 0;
static volatile uint32_t TxPeriodicity       = 0;

#ifdef RX_SENSE
static void OnRadioWifiDone(void);
static void OnRadioGnssDone(void);
#endif

/* Private user code ---------------------------------------------------------*/
static float pfData[3];
// static int32_t iddValue[2];
// static float   latitude, longitude, meters;

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    HAL_DBGMCU_EnableDBGStopMode();
    HAL_DBGMCU_EnableDBGStandbyMode();

    MX_GTZC_NS_Init();
    /*
     * Register SecureFault callback defined in non-secure
     * and to be called by secure handler
     */
    SECURE_RegisterCallback(SECURE_FAULT_CB_ID, (void *)SecureFault_Callback);

    /*
     * Register SecureError callback defined in non-secure
     * and to be called by secure handler
     */
    SECURE_RegisterCallback(GTZC_ERROR_CB_ID, (void *)SecureError_Callback);

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_ADC1_Init();
    MX_I2C1_Init();
    MX_USART3_UART_Init();
    MX_RTC_Init();
    MX_SPI1_Init();
    MX_SPI3_Init();
    MX_TIM1_Init();
    MX_ADC2_Init();
    MX_LPUART1_UART_Init();
    MX_CRC_Init();
    MX_RNG_Init();
    MX_UCPD1_Init();
    MX_USB_Device_Init();
    MX_USART2_UART_Init();
    MX_ICACHE_Init();
    RtcInit();
    BSP_GYRO_Init();
    BSP_IDD_Init(0);
    BSP_IDD_Init(1);
    SpiInit(&LR1110.spi, SPI_3, LR_MOSI_GPIO_Port, LR_MOSI_Pin, LR_MISO_GPIO_Port, LR_MISO_Pin, LR_SCK_GPIO_Port,
            LR_SCK_Pin, NULL, NC);
    lr1110_board_init_io(&LR1110);

    TimerInit(&Led1Timer, OnLed1TimerEvent);
    TimerSetValue(&Led1Timer, 25);
    TimerInit(&Led2Timer, OnLed2TimerEvent);
    TimerSetValue(&Led2Timer, 25);
    TimerInit(&LedBeaconTimer, OnLedBeaconTimerEvent);
    TimerSetValue(&LedBeaconTimer, 5000);

#ifdef TX_CW
    RadioEvents.TxTimeout = OnRadioTxTimeout;
    RadioEvents.TxDone    = OnRadioTxTimeout;
    Radio.Init(&RadioEvents);

    Radio.SetTxContinuousWave(RF_FREQUENCY, TX_OUTPUT_POWER, TX_TIMEOUT);
    TimerSetValue(&Led1Timer, 500);
    TimerSetValue(&Led2Timer, 200);
    TimerStart(&Led1Timer);
#elif RX_SENSE
    RadioEvents.RxDone    = OnRadioRxDone;
    RadioEvents.RxTimeout = OnRadioRxTimeout;
    RadioEvents.RxError   = OnRadioRxTimeout;
    RadioEvents.WifiDone  = OnRadioWifiDone;
    RadioEvents.GnssDone  = OnRadioGnssDone;
    Radio.Init(&RadioEvents);
    Radio.SetChannel(RF_FREQUENCY);
    Radio.SetRxConfig(MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR, LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                      LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON, 0, true, 0, 0, LORA_IQ_INVERSION_ON, true);
    Radio.Rx(0); // Continuous Rx
    TimerStart(&LedBeaconTimer);
#else
    /* Initialize transmission periodicity variable */
    TxPeriodicity = APP_TX_DUTYCYCLE + randr(-APP_TX_DUTYCYCLE_RND, APP_TX_DUTYCYCLE_RND);

    const Version_t appVersion    = { .Value = FIRMWARE_VERSION };
    const Version_t gitHubVersion = { .Value = GITHUB_VERSION };
    DisplayAppInfo("periodic-uplink-lpp", &appVersion, &gitHubVersion);

    if (LmHandlerInit(&LmHandlerCallbacks, &LmHandlerParams) != LORAMAC_HANDLER_SUCCESS)
    {
        printf("LoRaMac wasn't properly initialized\n");

        /* Fatal error, endless loop. */
        while (1)
        {
            /* Do nothing */
        }
    }

    /* Set system maximum tolerated rx error in milliseconds */
    LmHandlerSetSystemMaxRxError(20);

    /*
     * The LoRa-Alliance Compliance protocol package should always be
     * initialized and activated.
     */
    LmHandlerPackageRegister(PACKAGE_ID_COMPLIANCE, &LmhpComplianceParams);

    LmHandlerJoin();

    StartTxProcess(LORAMAC_HANDLER_TX_ON_TIMER);
#endif

    /* Infinite loop */
    while (1)
    {
#if defined(TX_CW) || defined(RX_SENSE)
        /* Process Radio IRQ */
        if (Radio.IrqProcess != NULL)
        {
            Radio.IrqProcess();
        }
        if (Led1TimerEvent == true)
        {
            Led1TimerEvent = false;
#if 0
            /* Switch LED 1 OFF */
            SECURE_LED_RED(1);

            /* Switch LED 2 ON */
            SECURE_LED_YELLOW(0);
#endif
            TimerStart(&Led2Timer);
        }

        if (Led2TimerEvent == true)
        {
            Led2TimerEvent = false;
#if 0
            /* Switch LED 2 OFF */
            SECURE_LED_YELLOW(1);

            /* Switch LED 1 ON */
            SECURE_LED_RED(0);
#endif

            TimerStart(&Led1Timer);
        }

        /* Process characters sent over the command line interface */
        CliProcess(&hlpuart1);

        /* Processes the LoRaMac events */
        LmHandlerProcess();

        /* Process application uplinks management */
        UplinkProcess();

        CRITICAL_SECTION_BEGIN();
        if (IsMacProcessPending == 1)
        {
            /* Clear flag and prevent MCU to go into low power modes. */
            IsMacProcessPending = 0;
        }
        else
        {
            /* The MCU wakes up through events */
            BoardLowPowerHandler();
        }
        CRITICAL_SECTION_END();
#endif

        BSP_GYRO_GetXYZ(pfData);
        printf("%d\t%f\t%f\t%f\n", 100, pfData[0], pfData[1], pfData[2]);
        HAL_Delay(10);
    }
}

/**
 * @brief  Callback called by secure code following a secure fault interrupt
 * @note   This callback is called by secure code thanks to the registration
 *         done by the non-secure application with non-secure callable API
 *         SECURE_RegisterCallback(SECURE_FAULT_CB_ID, (void *)SecureFault_Callback);
 * @retval None
 */
void SecureFault_Callback(void)
{
    /* Go to error infinite loop when Secure fault generated by IDAU/SAU check */
    /* because of illegal access */
    Error_Handler();
}

/**
 * @brief  Callback called by secure code following a GTZC TZIC secure interrupt (GTZC_IRQn)
 * @note   This callback is called by secure code thanks to the registration
 *         done by the non-secure application with non-secure callable API
 *         SECURE_RegisterCallback(GTZC_ERROR_CB_ID, (void *)SecureError_Callback);
 * @retval None
 */
void SecureError_Callback(void)
{
    /* Go to error infinite loop when Secure error generated by GTZC check */
    /* because of illegal access */
    Error_Handler();
}

/**
 * @brief Mac notification
 */
static void OnMacProcessNotify(void)
{
    IsMacProcessPending = 1;
}

/**
 * @brief dispaly nvm data change
 * @param state
 * @param size
 */
static void OnNvmDataChange(LmHandlerNvmContextStates_t state, uint16_t size)
{
    DisplayNvmDataChange(state, size);
}

/**
 * @brief display network parameter
 * @param params
 */
static void OnNetworkParametersChange(CommissioningParams_t *params)
{
    DisplayNetworkParametersUpdate(params);
}

/**
 * @brief display MAC MCP request
 * @param status
 * @param mcpsReq
 * @param nextTxIn
 */
static void OnMacMcpsRequest(LoRaMacStatus_t status, McpsReq_t *mcpsReq, TimerTime_t nextTxIn)
{
    DisplayMacMcpsRequestUpdate(status, mcpsReq, nextTxIn);
}

/**
 * @brief dispaly MAC MLME request
 * @param status
 * @param mlmeReq
 * @param nextTxIn
 */
static void OnMacMlmeRequest(LoRaMacStatus_t status, MlmeReq_t *mlmeReq, TimerTime_t nextTxIn)
{
    DisplayMacMlmeRequestUpdate(status, mlmeReq, nextTxIn);
}

/**
 * @brief Join request
 * @param params
 */
static void OnJoinRequest(LmHandlerJoinParams_t *params)
{
    DisplayJoinRequestUpdate(params);
    if (params->Status == LORAMAC_HANDLER_ERROR)
    {
        LmHandlerJoin();
    }
    else
    {
        LmHandlerRequestClass(LORAWAN_DEFAULT_CLASS);
    }
}

/**
 * @brief dispay tx data
 * @param params
 */
static void OnTxData(LmHandlerTxParams_t *params)
{
    DisplayTxUpdate(params);
}

/**
 * @brief dispaly rx data
 * @param appData
 * @param params
 */
static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params)
{
    DisplayRxUpdate(appData, params);

    switch (appData->Port)
    {
        /* The application LED can be controlled on port 1 or 2 */
        case 1:
        case LORAWAN_APP_PORT:
        {
            AppLedStateOn = appData->Buffer[0] & 0x01;
        }
        break;
        default:
            break;
    }

    /* Switch LED 2 ON for each received downlink */
    /* GpioWrite(&Led2, 1); */
    SECURE_LED_RED(true);
    TimerStart(&Led2Timer);
}

/**
 * @brief dispay device class
 * @param deviceClass
 */
static void OnClassChange(DeviceClass_t deviceClass)
{
    DisplayClassUpdate(deviceClass);

    /* Inform the server as soon as possible that the end-device has switched to ClassB */
    LmHandlerAppData_t appData = {
        .Buffer     = NULL,
        .BufferSize = 0,
        .Port       = 0,
    };
    LmHandlerSend(&appData, LORAMAC_HANDLER_UNCONFIRMED_MSG);
}

/**
 * @brief deacon status change
 * @param params
 */
static void OnBeaconStatusChange(LoRaMacHandlerBeaconParams_t *params)
{
    switch (params->State)
    {
        case LORAMAC_HANDLER_BEACON_RX:
        {
            TimerStart(&LedBeaconTimer);
            break;
        }
        case LORAMAC_HANDLER_BEACON_LOST:
        case LORAMAC_HANDLER_BEACON_NRX:
        {
            TimerStop(&LedBeaconTimer);
            break;
        }
        default:
        {
            break;
        }
    }

    DisplayBeaconUpdate(params);
}

#if (LMH_SYS_TIME_UPDATE_NEW_API == 1)
static void OnSysTimeUpdate(bool isSynchronized, int32_t timeCorrection) { }
#else
static void OnSysTimeUpdate(void) { }
#endif

#ifdef TX_CW
/**
 * @brief Prepares the payload of the frame and transmits it.
 */
static void PrepareTxFrame(void)
{

    if (LmHandlerIsBusy() == true)
    {
        return;
    }
    BSP_GYRO_GetXYZ(pfData);
    BSP_GYRO_LowPower();
    BSP_IDD_StartMeasurement(0);
    BSP_IDD_GetValue(0, (uint32_t *)&iddValue[0]);
    BSP_IDD_GetValue(1, (uint32_t *)&iddValue[1]);
    uint8_t channel = 0;

    AppData.Port = LORAWAN_APP_PORT;

    CayenneLppReset();
    CayenneLppAddDigitalInput(channel++, AppLedStateOn);
    CayenneLppAddAnalogInput(channel++, BoardGetBatteryLevel() * 100 / 254);
    CayenneLppAddAnalogInput(channel++, iddValue[0]);
    CayenneLppAddGyrometer(channel++, pfData[0], pfData[1], pfData[2]);
    CayenneLppAddGps(channel++, latitude, longitude, meters);
    CayenneLppCopy(AppData.Buffer);
    AppData.BufferSize = CayenneLppGetSize();

    if (LmHandlerSend(&AppData, LmHandlerParams.IsTxConfirmed) == LORAMAC_HANDLER_SUCCESS)
    {
#if 0
        /* Switch LED 1 ON */
        /* GpioWrite(&Led1, 1); */
        SECURE_LED_YELLOW(false);
#endif
        TimerStart(&Led1Timer);
    }
}
#endif

/**
 * @brief
 * @param txEvent
 */
static void StartTxProcess(LmHandlerTxEvents_t txEvent)
{
    switch (txEvent)
    {
        default:
            /* Intentional fall through */
        case LORAMAC_HANDLER_TX_ON_TIMER:
        {
            /* Schedule 1st packet transmission */
            TimerInit(&TxTimer, OnTxTimerEvent);
            TimerSetValue(&TxTimer, TxPeriodicity);
            OnTxTimerEvent(NULL);
        }
        break;
        case LORAMAC_HANDLER_TX_ON_EVENT:
        {
        }
        break;
    }
}

#ifdef TX_CW
/**
 * @brief uplink process
 */
static void UplinkProcess(void)
{
    uint8_t isPending = 0;
    CRITICAL_SECTION_BEGIN();
    isPending        = IsTxFramePending;
    IsTxFramePending = 0;
    CRITICAL_SECTION_END();
    if (isPending == 1)
    {
        PrepareTxFrame();
    }
}
#endif

/**
 * @brief tx periodicity change
 * @param periodicity
 */
static void OnTxPeriodicityChanged(uint32_t periodicity)
{
    TxPeriodicity = periodicity;

    if (TxPeriodicity == 0)
    {
        /* Revert to application default periodicity */
        TxPeriodicity = APP_TX_DUTYCYCLE + randr(-APP_TX_DUTYCYCLE_RND, APP_TX_DUTYCYCLE_RND);
    }
}

/**
 * @brief tx frame control change
 * @param isTxConfirmed
 */
static void OnTxFrameCtrlChanged(LmHandlerMsgTypes_t isTxConfirmed)
{
    LmHandlerParams.IsTxConfirmed = isTxConfirmed;
}

/**
 * @brief ping slot periodicity change
 * @param pingSlotPeriodicity
 */
static void OnPingSlotPeriodicityChanged(uint8_t pingSlotPeriodicity)
{
    LmHandlerParams.PingSlotPeriodicity = pingSlotPeriodicity;
}

/**
 * @brief Function executed on TxTimer event
 * @param context
 */
static void OnTxTimerEvent(void *context)
{
    TimerStop(&TxTimer);

    IsTxFramePending = 1;

    /* Schedule next transmission */
    TimerSetValue(&TxTimer, TxPeriodicity);
    TimerStart(&TxTimer);
}

/**
 * @brief  Function executed on Led 1 Timeout event
 * @param context
 */
static void OnLed1TimerEvent(void *context)
{
    Led1TimerEvent = true;
#if 0
    TimerStop(&Led1Timer);
    /* Switch LED 1 OFF */
    /* GpioWrite(&Led1, 0); */
    SECURE_LED_YELLOW(true);
#endif
}

/**
 * @brief Function executed on Led 2 Timeout event
 * @param context
 */
static void OnLed2TimerEvent(void *context)
{
    Led2TimerEvent = true;

#if 0
    TimerStop(&Led2Timer);
    /* Switch LED 2 OFF */
    /* GpioWrite(&Led2, 0); */
    SECURE_LED_RED(false);
#endif
}

#ifdef TX_CW
/**
 * @brief Function executed on Radio Tx Timeout event
 */
void OnRadioTxTimeout(void)
{
    /* Restarts continuous wave transmission when timeout expires */
    Radio.SetTxContinuousWave(RF_FREQUENCY, TX_OUTPUT_POWER, TX_TIMEOUT);
}
#elif defined(RX_SENSE)
void        OnRadioRxTimeout(void)
{
    Led1TimerEvent = true;
#if 0
    SECURE_LEDToggle_RED();
#endif
    Radio.Rx(0);
}

/**
 * @brief
 * @param context
 */
void OnRadioRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr)
{
    Led2TimerEvent = true;
#if 0
    SECURE_LEDToggle_YELLOW();

    if (size != 0)
    {
        printf("RX DATA     : \n");
        PrintHexBuffer(payload, size);
    }
    printf("\n");
    printf("RX RSSI     : %d\n", rssi);
    printf("RX SNR      : %d\n", snr);
#endif
    Radio.Rx(0);
}
#endif /* TX_CW */

/**
 * @brief Function executed on Beacon timer Timeout event
 * @param context
 */
static void OnLedBeaconTimerEvent(void *context)
{
#if 0
    /* GpioWrite(&Led2, 1); */
    SECURE_LED_RED(true);

    lr1110_wifi_scan(&LR1110, LR1110_WIFI_TYPE_SCAN_B_G_N, 0x3FFF, LR1110_WIFI_SCAN_MODE_BEACON, 3, 3, 500, true);

    TimerStart(&Led2Timer);
    TimerStart(&LedBeaconTimer);
#endif
}

#ifdef RX_SENSE
/**
 * @brief
 */
void OnRadioWifiDone(void)
{
    int32_t wn = 0;

    lr1110_wifi_get_nb_results(&LR1110, &LR1110.wifi.nb_results);
    lr1110_wifi_read_basic_complete_results(&LR1110, 0, LR1110.wifi.nb_results, LR1110.wifi.all_results);
    printf("WiFi scan num: %d\n\n", LR1110.wifi.nb_results);
    for (wn = 0; wn < LR1110.wifi.nb_results; ++wn)
    {
        printf("WiFi idx: %d Mac:", wn);
        PrintHexBuffer(LR1110.wifi.all_results[wn].mac_address, 6);
        printf("Beacon: %ul Channel_Info: 0x%X Data_Rate: 0X%X Frame_Type: 0x%X\n",
               LR1110.wifi.all_results[wn].beacon_period_tu, LR1110.wifi.all_results[wn].channel_info_byte,
               LR1110.wifi.all_results[wn].data_rate_info_byte, LR1110.wifi.all_results[wn].frame_type_info_byte);
        printf("Rssi: %d Phi_Offset: %d Timestamp: %llu\n\n", LR1110.wifi.all_results[wn].rssi,
               LR1110.wifi.all_results[wn].phi_offset, LR1110.wifi.all_results[wn].timestamp_us);
    }
}

/**
 * @brief
 */
void OnRadioGnssDone(void)
{
    /* Do nothing */
}
#endif

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
        SECURE_LEDToggle_RED();
        HAL_Delay(500);
    }
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /*
     * User can add his own implementation to report the file name and line number,
     * ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line)
     */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

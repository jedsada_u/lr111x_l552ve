/**
 ******************************************************************************
 * @file    gtzc.c
 * @brief   This file provides code for the configuration
 *          of the GTZC instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "gtzc.h"

/* GTZC_NS init function */
void MX_GTZC_NS_Init(void)
{
    /* Do nothing */
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

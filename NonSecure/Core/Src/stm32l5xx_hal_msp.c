/**
 ******************************************************************************
 * @file         stm32l5xx_hal_msp.c
 * @brief        This file provides code for the MSP Initialization
 *               and de-Initialization codes.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* External functions --------------------------------------------------------*/

/* Initializes the Global MSP */
void HAL_MspInit(void)
{
    /* MspInit 0 */
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();

    /*
     * System interrupt init
     * Peripheral interrupt init
     * FPU_IRQn interrupt configuration
     */
    HAL_NVIC_SetPriority(FPU_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(FPU_IRQn);
    HAL_NVIC_SetPriority(ICACHE_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ICACHE_IRQn);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

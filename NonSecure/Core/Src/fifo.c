/**
 * @file      fifo.h
 *
 * @brief     FIFO buffer implementation
 *
 * @copyright Revised BSD License, see section \ref LICENSE.
 *
 * @code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * @endcode
 *
 * @author    Miguel Luis ( Semtech )
 *
 * @author    Gregory Cristian ( Semtech )
 */

/* Includes ------------------------------------------------------------------*/
#include "fifo.h"

/* FIFO next function */
static uint16_t FifoNext(Fifo_t *fifo, uint16_t index)
{
    return (index + 1U) % fifo->Size;
}

/* FIFO init function */
void FifoInit(Fifo_t *fifo, uint8_t *buffer, uint16_t size)
{
    fifo->Begin = 0U;
    fifo->End   = 0U;
    fifo->Data  = buffer;
    fifo->Size  = size;
}

/* FIFO push function */
void FifoPush(Fifo_t *fifo, uint8_t data)
{
    fifo->End             = FifoNext(fifo, fifo->End);
    fifo->Data[fifo->End] = data;
}

/* FIFO pop function */
uint8_t FifoPop(Fifo_t *fifo)
{
    uint8_t data = fifo->Data[FifoNext(fifo, fifo->Begin)];

    fifo->Begin = FifoNext(fifo, fifo->Begin);
    return data;
}

/* FIFO flush function */
void FifoFlush(Fifo_t *fifo)
{
    fifo->Begin = 0U;
    fifo->End   = 0U;
}

/* FIFO is empty function */
bool IsFifoEmpty(Fifo_t *fifo)
{
    return (fifo->Begin == fifo->End);
}

/* FIFO is full function */
bool IsFifoFull(Fifo_t *fifo)
{
    return (FifoNext(fifo, fifo->End) == fifo->Begin);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

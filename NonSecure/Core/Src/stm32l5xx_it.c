/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    stm32l5xx_it.c
 * @brief   Interrupt Service Routines.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l5xx_it.h"

/* Private includes ----------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef  hpcd_USB_FS;
extern ADC_HandleTypeDef  hadc1;
extern ADC_HandleTypeDef  hadc2;
extern I2C_HandleTypeDef  hi2c1;
extern DMA_HandleTypeDef  hdma_lpuart1_rx;
extern DMA_HandleTypeDef  hdma_lpuart1_tx;
extern DMA_HandleTypeDef  hdma_usart3_rx;
extern DMA_HandleTypeDef  hdma_usart3_tx;
extern UART_HandleTypeDef hlpuart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern RNG_HandleTypeDef  hrng;
extern RTC_HandleTypeDef  hrtc;
extern SPI_HandleTypeDef  hspi1;
extern SPI_HandleTypeDef  hspi3;
extern TIM_HandleTypeDef  htim1;

/******************************************************************************/
/*           Cortex Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void)
{
    while (1)
    {
        /* Do nothing */
    }
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void)
{
    while (1)
    {
        /* Do nothing */
    }
}

/**
 * @brief This function handles System service call via SWI instruction.
 */
void SVC_Handler(void)
{
    /* Do nothing */
}

/**
 * @brief This function handles Pendable request for system service.
 */
void PendSV_Handler(void)
{
    /* Do nothing */
}

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void)
{
    /* SysTick_IRQn 0 */
    HAL_IncTick();
}

/******************************************************************************/
/* STM32L5xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l5xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles RTC non-secure interrupts through EXTI line 17.
 */
void RTC_IRQHandler(void)
{
    /* RTC_IRQn 0 */
    HAL_RTC_AlarmIRQHandler(&hrtc);
    HAL_RTCEx_TimeStampIRQHandler(&hrtc);
    HAL_RTCEx_WakeUpTimerIRQHandler(&hrtc);
}

#if 0
/**
 * @brief This function handles EXTI line2 interrupt.
 */
void EXTI2_IRQHandler(void)
{
    /* EXTI2_IRQn 0 */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

/**
 * @brief This function handles EXTI line3 interrupt.
 */
void EXTI3_IRQHandler(void)
{
    /* EXTI3_IRQn 0 */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

/**
 * @brief This function handles EXTI line14 interrupt.
 */
void EXTI14_IRQHandler(void)
{
    /* EXTI14_IRQn 0 */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
}
#endif

/**
 * @brief This function handles DMA1 channel5 global interrupt.
 */
void DMA1_Channel5_IRQHandler(void)
{
    /* DMA1_Channel5_IRQn 0 */
    HAL_DMA_IRQHandler(&hdma_lpuart1_rx);
}

/**
 * @brief This function handles DMA1 channel6 global interrupt.
 */
void DMA1_Channel6_IRQHandler(void)
{
    /* DMA1_Channel6_IRQn 0 */
    HAL_DMA_IRQHandler(&hdma_lpuart1_tx);
}

/**
 * @brief This function handles ADC1 and ADC2 interrupts.
 */
void ADC1_2_IRQHandler(void)
{

    /* ADC1_2_IRQn 0 */
    HAL_ADC_IRQHandler(&hadc1);
    HAL_ADC_IRQHandler(&hadc2);
}

/**
 * @brief This function handles TIM1 break interrupt.
 */
void TIM1_BRK_IRQHandler(void)
{
    /* TIM1_BRK_IRQn 0 */
    HAL_TIM_IRQHandler(&htim1);
}

/**
 * @brief This function handles TIM1 update interrupt.
 */
void TIM1_UP_IRQHandler(void)
{
    /* TIM1_UP_IRQn 0 */
    HAL_TIM_IRQHandler(&htim1);
}

/**
 * @brief This function handles TIM1 trigger and commutation interrupts.
 */
void TIM1_TRG_COM_IRQHandler(void)
{
    /* TIM1_TRG_COM_IRQn 0 */
    HAL_TIM_IRQHandler(&htim1);
}

/**
 * @brief This function handles TIM1 capture compare interrupt.
 */
void TIM1_CC_IRQHandler(void)
{
    /* TIM1_CC_IRQn 0 */
    HAL_TIM_IRQHandler(&htim1);
}

/**
 * @brief This function handles I2C1 event interrupt / I2C1 wake-up interrupt through EXTI line 23.
 */
void I2C1_EV_IRQHandler(void)
{
    /* I2C1_EV_IRQn 0 */
    HAL_I2C_EV_IRQHandler(&hi2c1);
}

/**
 * @brief This function handles I2C1 error interrupt.
 */
void I2C1_ER_IRQHandler(void)
{
    /* I2C1_ER_IRQn 0 */
    HAL_I2C_ER_IRQHandler(&hi2c1);
}

/**
 * @brief This function handles SPI1 global interrupt.
 */
void SPI1_IRQHandler(void)
{
    /* SPI1_IRQn 0 */
    HAL_SPI_IRQHandler(&hspi1);
}

/**
 * @brief This function handles USART2 global interrupt / USART2 wake-up interrupt through EXTI line 27.
 */
void USART2_IRQHandler(void)
{
    /* USART2_IRQn 0 */
    HAL_UART_IRQHandler(&huart2);
}

/**
 * @brief This function handles USART3 global interrupt / USART3 wake-up interrupt through EXTI line 28.
 */
void USART3_IRQHandler(void)
{
    /* USART3_IRQn 0 */
    HAL_UART_IRQHandler(&huart3);
}

/**
 * @brief This function handles LPUART1 global interrupt / LPUART1 wake-up interrupt through EXTI line 31.
 */
void LPUART1_IRQHandler(void)
{
    /* LPUART1_IRQn 0 */
    HAL_UART_IRQHandler(&hlpuart1);
}

/**
 * @brief This function handles USB FS global interrupt / USB FS wake-up interrupt through EXTI line 34.
 */
void USB_FS_IRQHandler(void)
{
    /* USB_FS_IRQn 0 */
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

/**
 * @brief This function handles DMA2 channel2 global interrupt.
 */
void DMA2_Channel2_IRQHandler(void)
{
    /* DMA2_Channel2_IRQn 0 */
    HAL_DMA_IRQHandler(&hdma_usart3_rx);
}

/**
 * @brief This function handles DMA2 channel3 global interrupt.
 */
void DMA2_Channel3_IRQHandler(void)
{
    /* DMA2_Channel3_IRQn 0 */
    HAL_DMA_IRQHandler(&hdma_usart3_tx);
}

/**
 * @brief This function handles RNG global interrupt.
 */
void RNG_IRQHandler(void)
{
    /* RNG_IRQn 0 */
    HAL_RNG_IRQHandler(&hrng);
}

/**
 * @brief This function handles FPU global interrupt.
 */
void FPU_IRQHandler(void)
{
    /* Do nothing */
}

/**
 * @brief This function handles SPI3 global interrupt.
 */
void SPI3_IRQHandler(void)
{
    /* SPI3_IRQn 0 */
    HAL_SPI_IRQHandler(&hspi3);
}

/**
 * @brief This function handles Instruction cache global interrupt.
 */
void ICACHE_IRQHandler(void)
{
    /* ICACHE_IRQn 0 */
    HAL_ICACHE_IRQHandler();
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

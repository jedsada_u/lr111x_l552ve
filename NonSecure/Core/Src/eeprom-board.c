/**
 * @file      eeprom-board.c
 *
 * @brief     Target board EEPROM driver implementation
 *
 * @copyright Revised BSD License, see section \ref LICENSE.
 *
 * @code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * @endcode
 *
 * @author    Miguel Luis ( Semtech )
 *
 * @author    Gregory Cristian ( Semtech )
 */
#include <stdint.h>
/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include "stm32l5xx.h"
#include "eeprom_emul.h"
#include "eeprom-board.h"
#include "utilities.h"

/* Private variables ---------------------------------------------------------*/
uint16_t EepromVirtualAddress[NB_OF_VARIABLES];
__IO uint32_t ErasingOnGoing = 0U;

/* Initializes the EEPROM emulation module */
void EepromMcuInit(void)
{
    EE_Status eeStatus = EE_OK;
    uint16_t varValue  = 0U;

    /* Unlock the Flash Program Erase controller */
    HAL_FLASH_Unlock();

    /* Set user List of Virtual Address variables: 0x0000 and 0xFFFF values are prohibited */
    for (varValue = 0U; varValue < NB_OF_VARIABLES; varValue++)
    {
        EepromVirtualAddress[varValue] = varValue + 1U;
    }

    /*
     * Set EEPROM emulation firmware to erase all potentially incompletely erased
     * pages if the system came from an asynchronous reset. Conditional erase is
     * safe to use if all Flash operations where completed before the system reset
     */
    if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) == RESET)
    {
        /*
         * System reset comes from a power-on reset: Forced Erase
         * Initialize EEPROM emulation driver (mandatory)
         */
        eeStatus = EE_Init(EepromVirtualAddress, EE_FORCED_ERASE);
        if (eeStatus != EE_OK)
        {
            assert_param(FAIL);
        }
    }
    else
    {
        /* Clear the Standby flag */
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

        /* Check and Clear the Wakeup flag */
        if (__HAL_PWR_GET_FLAG(PWR_FLAG_WUF1) != RESET)
        {
            __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
        }

        /*
         * System reset comes from a STANDBY wakeup: Conditional Erase
         * Initialize EEPROM emulation driver (mandatory)
         */
        eeStatus = EE_Init(EepromVirtualAddress, EE_CONDITIONAL_ERASE);
        if (eeStatus != EE_OK)
        {
            assert_param(FAIL);
        }
    }

    /* Lock the Flash Program Erase controller */
    HAL_FLASH_Lock();
}

/* EEPROM erase function */
bool EepromMcuIsErasingOnGoing(void)
{
    return ErasingOnGoing;
}

/* EEPROM write function */
uint8_t EepromMcuWriteBuffer(uint16_t addr, uint8_t *buffer, uint16_t size)
{
    uint8_t status     = SUCCESS;
    EE_Status eeStatus = EE_OK;
    uint32_t i         = 0U;

    /* Unlock the Flash Program Erase controller */
    HAL_FLASH_Unlock();

    CRITICAL_SECTION_BEGIN();
    for (i = 0U; i < size; i++)
    {
        eeStatus |= EE_WriteVariable8bits(EepromVirtualAddress[addr + i], buffer[i]);
    }
    CRITICAL_SECTION_END();

    if (eeStatus != EE_OK)
    {
        status = FAIL;
    }

    if ((eeStatus & EE_STATUSMASK_CLEANUP) == EE_STATUSMASK_CLEANUP)
    {
        ErasingOnGoing = 0U;
        eeStatus |= EE_CleanUp();
    }
    if ((eeStatus & EE_STATUSMASK_ERROR) == EE_STATUSMASK_ERROR)
    {
        status = FAIL;
    }

    /* Lock the Flash Program Erase controller */
    HAL_FLASH_Lock();
    return status;
}

/* EEPROM write function */
uint8_t EepromMcuReadBuffer(uint16_t addr, uint8_t *buffer, uint16_t size)
{
    uint8_t status = SUCCESS;
    uint32_t i     = 0U;

    /* Unlock the Flash Program Erase controller */
    HAL_FLASH_Unlock();

    for (i = 0U; i < size; i++)
    {
        if (EE_ReadVariable8bits(EepromVirtualAddress[addr + i], buffer + i) != EE_OK)
        {
            status = FAIL;
            break;
        }
    }

    /* Lock the Flash Program Erase controller */
    HAL_FLASH_Lock();
    return status;
}

/* EEPROM set device address function */
void EepromMcuSetDeviceAddr(uint8_t addr)
{
    assert_param(FAIL);
}

/* EEPROM get device address function */
uint8_t EepromMcuGetDeviceAddr(void)
{
    assert_param(FAIL);

    return 0U;
}

/* EEPROM callback function */
void HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
    /* Call CleanUp callback when all requested pages have been erased */
    if (ReturnValue == 0xFFFFFFFFU)
    {
        EE_EndOfCleanup_UserCallback();
    }
}

/* EEPROM clean up function */
void EE_EndOfCleanup_UserCallback(void)
{
    ErasingOnGoing = 0U;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

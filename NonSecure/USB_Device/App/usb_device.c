/**
 ******************************************************************************
 * @file           : usb_device.c
 * @version        : v3.0_Cube
 * @brief          : This file implements the USB Device
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "usb_device.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "gpio.h"

/* Private variables ---------------------------------------------------------*/
/* USB Device Core handle declaration. */
USBD_HandleTypeDef             hUsbDeviceFS;
extern USBD_DescriptorsTypeDef CDC_Desc;
extern USBD_StatusTypeDef      USBD_LL_BatterryCharging(USBD_HandleTypeDef *pdev);
Gpio_t                         bcd;

/* Private function prototypes -----------------------------------------------*/
extern void Error_Handler(void);

/**
 * Init USB device Library, add supported class and start the library
 * @retval None
 */
void MX_USB_Device_Init(void)
{
    /* Init Device Library, add supported class and start the library. */
    if (USBD_Init(&hUsbDeviceFS, &CDC_Desc, DEVICE_FS) != USBD_OK)
    {
        Error_Handler();
    }

    if (USBD_RegisterClass(&hUsbDeviceFS, &USBD_CDC) != USBD_OK)
    {
        Error_Handler();
    }

    if (USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS) != USBD_OK)
    {
        Error_Handler();
    }

    /*
     * Verify if the Battery Charging Detection mode (BCD) is used :
     * If yes, the USB device is started in the HAL_PCDEx_BCD_Callback
     * upon reception of PCD_BCD_DISCOVERY_COMPLETED message.
     */
    if (USBD_LL_BatterryCharging(&hUsbDeviceFS) != USBD_OK)
    {
        Error_Handler();
    }
}

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

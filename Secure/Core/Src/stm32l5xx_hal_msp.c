/**
 ******************************************************************************
 * @file         stm32l5xx_hal_msp.c
 * @brief        This file provides code for the MSP Initialization
 *               and de-Initialization codes.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
 * Initializes the Global MSP.
 */
void HAL_MspInit(void)
{
    /* Peripheral interrupt init */
    PWR_PVDTypeDef sConfigPVD = { 0 };
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_RCC_GTZC_CLK_ENABLE();

    /* GTZC_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(GTZC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(GTZC_IRQn);

    /* FPU_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(FPU_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(FPU_IRQn);

    /*
     * PVD Configuration
     */
    sConfigPVD.PVDLevel = PWR_PVDLEVEL_0;
    sConfigPVD.Mode     = PWR_PVD_MODE_NORMAL;
    HAL_PWR_ConfigPVD(&sConfigPVD);

    /*
     * Enable the PVD Output
     */
    HAL_PWR_EnablePVD();

    /*
     * PWR Non-Privilege/Non-Secure Items Configurations
     */
    HAL_PWR_ConfigAttributes(PWR_WKUP1, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_WKUP2, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_WKUP3, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_WKUP4, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_WKUP5, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_VDM, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_APC, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_LPM, PWR_NSEC | PWR_NPRIV);
    HAL_PWR_ConfigAttributes(PWR_VB, PWR_NSEC | PWR_NPRIV);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

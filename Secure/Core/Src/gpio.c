/**
 ******************************************************************************
 * @file    gpio.c
 * @brief   This file provides code for the configuration
 *          of all used GPIO pins.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/

/**
 * Configure pins
 * PH0-OSC_IN (PH0)    ------> RCC_OSC_IN
 * PH1-OSC_OUT (PH1)   ------> RCC_OSC_OUT
 * PA0                 ------> PWR_WKUP1
 * PA13 (JTMS/SWDIO)   ------> DEBUG_JTMS-SWDIO
 * PA14 (JTCK/SWCLK)   ------> DEBUG_JTCK-SWCLK
 * PB3 (JTDO/TRACESWO) ------> DEBUG_JTDO-SWO
 */

/* GPIO init function */
void MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /* Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(CURRENT_CS_GPIO_Port, CURRENT_CS_Pin, GPIO_PIN_SET);

    /* Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(LDO_EN_GPIO_Port, LDO_EN_Pin, GPIO_PIN_SET);

    /* Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOE, WP_Pin | GYO_DEN_Pin, GPIO_PIN_RESET);

    /* IO attributes management functions */
    HAL_GPIO_ConfigPinAttributes(GPIOE,
                                 GYO_INT1_Pin  |
                                 GYO_INT2_Pin  |
                                 ENCODER_P_Pin |
                                 ENCODER_N_Pin,
                                 GPIO_PIN_NSEC);

    /* IO attributes management functions */
    HAL_GPIO_ConfigPinAttributes(GPIOA,
                                 MB1_DE_Pin       |
                                 CURRENT_SCK_Pin  |
                                 CURRENT_MISO_Pin |
                                 CURRENT_AN_Pin   |
                                 GPIO_PIN_11      |
                                 GPIO_PIN_12      |
                                 GPIO_PIN_13      |
                                 GPIO_PIN_14      |
                                 OTG_FS_CC1_Pin,
                                 GPIO_PIN_NSEC);

    /* IO attributes management functions */
    HAL_GPIO_ConfigPinAttributes(GPIOC,
                                 EXT_TX_Pin  |
                                 EXT_RX_Pin  |
                                 LR_SCK_Pin  |
                                 LR_MISO_Pin |
                                 LR_MOSI_Pin,
                                 GPIO_PIN_NSEC);

    /* IO attributes management functions */
    HAL_GPIO_ConfigPinAttributes(GPIOB,
                                 EXT_AN_Pin  |
                                 DBG_RX_Pin  |
                                 DBG_TX_Pin  |
                                 GPIO_PIN_14 |
                                 GPIO_PIN_15 |
                                 GPIO_PIN_3  |
                                 GPIO_PIN_5,
                                 GPIO_PIN_NSEC);

    /* IO attributes management functions */
    HAL_GPIO_ConfigPinAttributes(GPIOD,
                                 LR_RFSW2_Pin |
                                 LR_RFSW3_Pin |
                                 LR_IRQ_Pin   |
                                 LR_NSS_Pin   |
                                 LR_BUSY_Pin  |
                                 LR_NRST_Pin,
                                 GPIO_PIN_NSEC);

    /* Configure GPIO pin : PtPin */
    GPIO_InitStruct.Pin   = CURRENT_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(CURRENT_CS_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pins : PDPin PDPin PDPin PDPin PDPin */
    GPIO_InitStruct.Pin  = S1_Pin | S2_Pin | S3_Pin | S4_Pin | S5_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* Configure GPIO pin : PtPin */
    GPIO_InitStruct.Pin   = LDO_EN_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LDO_EN_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pins : PBPin PBPin */
    GPIO_InitStruct.Pin       = EXT_SCL_Pin | EXT_SDA_Pin;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Configure GPIO pins : PEPin PEPin */
    GPIO_InitStruct.Pin   = WP_Pin | GYO_DEN_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
